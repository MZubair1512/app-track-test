///<reference types="Cypress" />

describe('Login functionality', () => {
  it('Should login successfully with correct credentials', () => {
    cy.visit('http://localhost:5173/login')
    cy.get('#email').type('asghar@gmail.com')
    cy.get('#password').type('asghar123')
  })

  it('should show error (for incorrect password)', () => {
    cy.visit('http://localhost:5173/login')
    cy.get('#email').type('asghar@gmail.com')
    cy.get('#password').type('81ash999999')
  })

})